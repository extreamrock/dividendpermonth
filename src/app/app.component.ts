import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  calculatedText = 'คำนวณ';
  clearText = 'ล้าง';
  percentPerYearText = 'ผลตอบแทนต่อปีที่คาดว่าจะได้รับ =';
  dividendPerMonthText = 'เงินปันผลที่ต้องการ/เดือน =';
  dividendPerMonthProjectText = 'เงินปันผลที่ต้องการต่อเดือน อยากรู้ว่าต้องมีเงินสดอยู่เท่าไหร่ถึงจะได้เงินปันผลเท่านี้';
  dividendPerMonth: string;
  baht: string;
  money: string;
  dividendPerMonthForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.createCalculatedForm();
  }

  private createCalculatedForm() {
    this.dividendPerMonthForm = this.fb.group({
      dividendPerMonth: ['', Validators.required],
      percentPerYear: ['', [Validators.required, Validators.min(1)]]
    });
  }

  onCalculated(dividendPerMonthForm) {
    this.dividendPerMonth = String((((dividendPerMonthForm.dividendPerMonth) * 12) * 100) / dividendPerMonthForm.percentPerYear);
    return [this.dividendPerMonth, this.money = 'เงินต้นที่ต้องมี = ', this.baht = ' บาท'];
  }

  onClear() {
    this.dividendPerMonthForm.controls.dividendPerMonth.setValue('');
    this.dividendPerMonthForm.controls.percentPerYear.setValue('');
    this.dividendPerMonth = null;
    this.money = null;
    this.baht = null;
  }

  onFormChange() {
    return this.dividendPerMonthForm.controls.dividendPerMonth.value !== '' ||
      this.dividendPerMonthForm.controls.percentPerYear.value !== '';
  }
}
